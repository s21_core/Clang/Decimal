//tests for s21_decimal_mul()
#suite s21_decimal_mul

#test s21_decimal_mul_1
  s21_decimal a = {{500,127,0,0x80060000}};
  s21_decimal b = {{500,2281,0,0x80060000}};
  s21_decimal res;
  int ret = s21_mul(a,b,&res);
  uint32_t expected[] = {0x0003d090,0x00125f20,0x00046b97,0x000c0000};
  ck_assert_int_eq(ret,0);
  ck_assert_mem_eq(expected,&res,4*sizeof(uint32_t));


#test s21_decimal_mul_2
  s21_decimal a = {{500,127,0,0x80000000}};
  s21_decimal b = {{500,2281,0,0}};
  s21_decimal res;
  int ret = s21_mul(a,b,&res);
  // s21_print_decBits(&res);
  uint32_t expected[] = {0x0003d090,0x00125f20,0x00046b97,0x80000000};
  ck_assert_int_eq(ret,0);
  ck_assert_mem_eq(expected,&res,4*sizeof(uint32_t));

#test s21_decimal_mul_3
  s21_decimal a = {{12345678,987654321,0,0x80000000}};
  s21_decimal b = {{12345678,987654321,0,0}};
  s21_decimal res;
  int ret = s21_mul(a,b,&res);
  ck_assert_int_eq(ret,2);

#test s21_decimal_mul_4
  s21_decimal a = {{0x0edcba98,0x0000abcd,0,0x80000000}};
  s21_decimal b = {{0x0edcba98,0x000f1e2,0,0}};
  s21_decimal res;
  int ret = s21_mul(a,b,&res);
  uint32_t expected[] = {0xdd413a40,0x42c7a8a1,0xa253bffe,0x80000000};
  ck_assert_int_eq(ret,0);
  ck_assert_mem_eq(expected,&res,4*sizeof(uint32_t));

#test s21_decimal_mul_5
  s21_decimal a = {{12345678,987654321,0,0}};
  s21_decimal b = {{12345678,987654321,0,0}};
  s21_decimal res;
  int ret = s21_mul(a,b,&res);
  ck_assert_int_eq(ret,1);

#test s21_decimal_mul_6
  s21_decimal a = {{0x0edcba98,0,0,0x80000000}};
  s21_decimal b = {{0,0x0edcba98,0,0x80000000}};
  s21_decimal res;
  int ret = s21_mul(a,b,&res);
  uint32_t expected[] = {0x00000000,0xdd413a40,0x00dce2b9,0x00000000};
  ck_assert_int_eq(ret,0);
  ck_assert_mem_eq(expected,&res,4*sizeof(uint32_t));

#test s21_decimal_mul_7
  s21_decimal a = {{0x00000abab,0,0,0x800f0000}};
  s21_decimal b = {{0,0,0x0000abab,0x00050000}};
  s21_decimal res;
  int ret = s21_mul(a,b,&res);
  uint32_t expected[] = {0x00000000,0x00000000,0x731de439,0x80140000};
  ck_assert_int_eq(ret,0);
  ck_assert_mem_eq(expected,&res,4*sizeof(uint32_t));



#test s21_decimal_mul_13
  // fail, rounded last digit
  s21_decimal a = {{0x000cace2,0x0f4cace2,0x004cace2,0x001c0000}};
  s21_decimal b = {{0x000cace2,0x0f4cace2,0x004cace2,0x001c0000}};
  s21_decimal res;
  int ret = s21_mul(a,b,&res);
  uint32_t expected[] = {0xf9d4093e,0x12532ef2,0x0000b5f3,0x001c0000};
  ck_assert_int_eq(ret,0);
  ck_assert_mem_eq(expected,&res,4*sizeof(uint32_t));

#test s21_decimal_mul_14
// fail, rounded last digit
  s21_decimal a = {{12345678,987654321,0,0x801c0000}};
  s21_decimal b = {{12345678,987654321,0,0}};
  s21_decimal res;
  int ret = s21_mul(a,b,&res);
  uint32_t expected[] = {0x53ad1721,0xcbafa6a6,0x3a245ac1,0x80130000};
  ck_assert_int_eq(ret,0);
  ck_assert_mem_eq(expected,&res,4*sizeof(uint32_t));

#test s21_decimal_mul_15
  s21_decimal a = {{ 0xffffffff,0xffffffff,0xffffffff,0x00010000 }};
  s21_decimal b = {{ 0x0000000b,0x00000000,0x00000000,0x00000000 }};
  s21_decimal res;
  int s21_ret = s21_mul(a,b,&res);
  int expected_ret = 1;
//int mem_cmp = memcmp(res.bits,expected.bits,4*sizeof(uint32_t));
//printf("%s",(mem_cmp)?"Different\n":"Same\n");
  ck_assert_int_eq(s21_ret,expected_ret); 

  #test s21_decimal_mul_16
  s21_decimal a = {{ 0xffffffff,0xffffffff,0xffffffff,0x00010000 }};
  s21_decimal b = {{ 0x0000000b,0x00000000,0x00000000,0x80000000 }};
  s21_decimal res;
  int s21_ret = s21_mul(a,b,&res);
  int expected_ret = 2;
//int mem_cmp = memcmp(res.bits,expected.bits,4*sizeof(uint32_t));
//printf("%s",(mem_cmp)?"Different\n":"Same\n");
  ck_assert_int_eq(s21_ret,expected_ret); 

  #test s21_decimal_mul_17
  s21_decimal a = {{ 0xffffffff,0xffffffff,0xffffffff,0x80010000 }};
  s21_decimal b = {{ 0x0000000b,0x00000000,0x00000000,0x80000000 }};
  s21_decimal res;
  int s21_ret = s21_mul(a,b,&res);
  int expected_ret = 1;
//int mem_cmp = memcmp(res.bits,expected.bits,4*sizeof(uint32_t));
//printf("%s",(mem_cmp)?"Different\n":"Same\n");
  ck_assert_int_eq(s21_ret,expected_ret); 


#test s21_decimal_mul_0
s21_decimal a = {{ 0xffffffff,0xffffffff,0xffffffff,0x00000000 }};
  s21_decimal b = {{ 0xffffffff,0xffffffff,0xffffffff,0x00000000 }};
  s21_decimal res;
int s21_ret = s21_mul(a,b,&res);
  int expected_ret = 1;
//int mem_cmp = memcmp(res.bits,expected.bits,4*sizeof(uint32_t));
//printf("%s",(mem_cmp)?"Different\n":"Same\n");
  ck_assert_int_eq(s21_ret,expected_ret);