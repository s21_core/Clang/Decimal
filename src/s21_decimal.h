#ifndef S21_DECIMAL
#define S21_DECIMAL

#include <ctype.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define S21_DECIMAL_MANTISSA_SZ 3

typedef struct {
  unsigned int bits[4];
} s21_decimal;

#define is_decimal(x)                \
  _Generic((x), s21_decimal          \
           : (_Bool)1, s21_decimal * \
           : (_Bool)1, default       \
           : (_Bool)0)

// decimal.c
s21_decimal *s21_decimal_init();
s21_decimal *s21_copy_decimal(s21_decimal *src);
void s21_print_decBits(s21_decimal *dst);
void s21_print_arrBits(uint32_t *a, size_t sz);
int s21_absi(int x);
int s21_toggle(uint32_t *x, unsigned int n);
int s21_abs_decimal(s21_decimal *x);
int s21_negative_complimentary_decimal(s21_decimal *x);
int s21_decimal_pow_equalizer(s21_decimal *a, s21_decimal *b);
int s21_set_pow(s21_decimal *x, unsigned short pow);
unsigned short s21_decimal_get_pow(s21_decimal x);
void s21_put_into_decimal(s21_decimal *decimal, unsigned int *b);
int s21_decimal_x10(s21_decimal **decimal);
int s21_decimal_pow_equalizer(s21_decimal *a, s21_decimal *b);
int s21_get_bit_32(unsigned int x, unsigned int n);
int s21_set_bit_32(unsigned int *x, unsigned int n);
int s21_clear_bit_32(unsigned int *x, unsigned int n);
int s21_decimal_is_negative(s21_decimal x);
uint32_t *cpy_arr(uint32_t *arr, size_t res_sz);
int s21_mul_big(s21_decimal value_1, s21_decimal value_2, s21_decimal *result);
int s21_decimal_is_equal_to_zero(s21_decimal *x);
void s21_arr_to_zero(uint32_t *arr, size_t sz);
int s21_div10_any_size(uint32_t *x, size_t x_size, uint32_t *result,
                       size_t r_size, int *remainder);
int s21_bit_count(uint32_t *a, size_t a_sz);
int s21_stick_into_decimal(uint32_t *arr, size_t arr_sz, int *pow,
                           s21_decimal *result);
int s21_decimal_add_int(s21_decimal dec, int a, s21_decimal *res);
void s21_decimal_set_to_zero(s21_decimal *value);
int get_shift(s21_decimal val_1, s21_decimal val_2, int val_2_size);

// decimal_convert.c
int s21_from_int_to_decimal(int src, s21_decimal *dst);
int s21_from_float_to_decimal(float src, s21_decimal *dst);
int s21_from_decimal_to_int(s21_decimal src, int *dst);
int s21_from_decimal_to_float(s21_decimal src, float *dst);
// decimal_another_f.c
int s21_truncate(s21_decimal value, s21_decimal *result);
int s21_negate(s21_decimal value, s21_decimal *result);
int s21_floor(s21_decimal value, s21_decimal *result);
int s21_round(s21_decimal value, s21_decimal *result);

// decimal_math.c
int s21_add(s21_decimal value_1, s21_decimal value_2, s21_decimal *result);
int s21_sub(s21_decimal value_1, s21_decimal value_2, s21_decimal *result);
int s21_mul(s21_decimal value_1, s21_decimal value_2, s21_decimal *result);
int s21_div(s21_decimal value_1, s21_decimal value_2, s21_decimal *result);

int s21_decimal_shift_bits(s21_decimal *shifted_val_1, int bit_shift);
int s21_highest_bit_present(uint32_t *a, size_t a_sz);
int s21_add_any_size(uint32_t *a, size_t a_sz, uint32_t *b, size_t b_sz,
                     uint32_t *res, size_t res_sz);
int s21_mul_any_size(uint32_t *a, size_t a_sz, uint32_t *b, size_t b_sz,
                     uint32_t *res, size_t res_sz);
int s21_decimal_shift_bits(s21_decimal *decimal, int bit_shift);
int s21_shift_bits_any_sz(uint32_t *a, size_t a_sz, int bit_shift);
int s21_is_dec_even(s21_decimal dec);
int s21_decimal_banking_round_to_fit(s21_decimal *dec, int rem);

int s21_add_big(s21_decimal value_1, s21_decimal value_2, s21_decimal *result);
int s21_pow_equalizer(uint32_t *a, int *pow_a, uint32_t *b, int *pow_b);

// decimal_compare.c
int s21_is_less(s21_decimal x, s21_decimal y);
int s21_is_less_or_equal(s21_decimal x, s21_decimal y);
int s21_is_greater(s21_decimal x, s21_decimal y);
int s21_is_greater_or_equal(s21_decimal x, s21_decimal y);
int s21_is_equal(s21_decimal x, s21_decimal y);
int s21_is_not_equal(s21_decimal x, s21_decimal y);

int s21_decimal_add_int(s21_decimal dec, int a, s21_decimal *res);
void s21_decimal_set_to_zero(s21_decimal *value);
int get_shift(s21_decimal val_1, s21_decimal val_2, int val_2_size);

#endif
