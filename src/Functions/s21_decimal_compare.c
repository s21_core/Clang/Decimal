#include "../s21_decimal.h"

int s21_is_less(s21_decimal x, s21_decimal y) {
  int res = 0;
  int x_pow, y_pow;
  int not_a_break = 0;
  if ((x.bits[3] >> 31) > (y.bits[3] >> 31)) {
    if (s21_decimal_is_equal_to_zero(&x) && s21_decimal_is_equal_to_zero(&y))
      res = 0;
    else
      res = 1;
  } else if ((x.bits[3] >> 31) && (y.bits[3] >> 31)) {
    s21_negate(x, &x);
    s21_negate(y, &y);
    res = s21_is_less(y, x);
  } else if (!(x.bits[3] >> 31) && !(y.bits[3] >> 31)) {
    s21_decimal_pow_equalizer(&x, &y);
    x_pow = s21_decimal_get_pow(x);
    y_pow = s21_decimal_get_pow(y);
    if (x_pow > y_pow) {
      res = 1;
    } else {
      for (int i = 2; i >= 0 && !not_a_break; i--) {
        for (int j = 31; j >= 0 && !not_a_break; j--) {
          if (((x.bits[i] >> j) & 1) < ((y.bits[i] >> j) & 1)) {
            res = 1;
            not_a_break = 1;
          } else if (((x.bits[i] >> j) & 1) > ((y.bits[i] >> j) & 1)) {
            not_a_break = 1;
          }
        }
      }
    }
  }
  return res;
}

int s21_is_less_or_equal(s21_decimal x, s21_decimal y) {
  int res = s21_is_equal(x, y);
  if (!res) res = s21_is_less(x, y);
  return res;
}

int s21_is_greater(s21_decimal x, s21_decimal y) {
  return !s21_is_less_or_equal(x, y);
}

int s21_is_greater_or_equal(s21_decimal x, s21_decimal y) {
  return !s21_is_less(x, y);
}

int s21_is_equal(s21_decimal x, s21_decimal y) {
  int res = 1;
  int x_pow, y_pow;
  if ((x.bits[3] >> 31) != (y.bits[3] >> 31)) {
    if (s21_decimal_is_equal_to_zero(&x) && s21_decimal_is_equal_to_zero(&y))
      res = 1;
    else
      res = 0;
  } else {
    s21_decimal_pow_equalizer(&x, &y);
    x_pow = s21_decimal_get_pow(x);
    y_pow = s21_decimal_get_pow(y);
    if (x_pow != y_pow) {
      res = 0;
    } else {
      for (int i = 2; i >= 0; i--) {
        if (x.bits[i] != y.bits[i]) res = 0;
      }
    }
  }

  return res;
}

int s21_is_not_equal(s21_decimal x, s21_decimal y) {
  return !s21_is_equal(x, y);
}
