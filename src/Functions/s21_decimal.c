#include "../s21_decimal.h"
/**
 * @brief Prints bits of s21_decimal number as HEX in 1st row(from low to high
 * bytes) and it's binary representation, rows 0-2 contains integer part of
 * number, row 3 contains exponent and sign
 * @param dst s21_decimal number
 */

int s21_absi(int x) { return ((x + (x >> 31)) ^ (x >> 31)); }

int s21_abs_decimal(s21_decimal *x) {
  x->bits[3] &= ~(1 << 31);
  return 0;
}

int s21_toggle(uint32_t *x, unsigned n) {
  if (n > 31) return 0;
  *x ^= 1 << n;
  return 1;
}

/**
 * @return n'th(0-31) bit from 32-bit integet
 * @retval -1 - error,
 */
int s21_get_bit_32(unsigned int x, unsigned int n) {
  if (n > 31) return -1;
  return (x >> n) & 1;
}

int s21_set_bit_32(unsigned int *x, unsigned int n) {
  if (n > 31) return -1;
  *x |= 1 << n;
  return 0;
}

int s21_clear_bit_32(unsigned int *x, unsigned int n) {
  if (n > 31) return -1;
  *x &= ~(1 << n);
  return 0;
}

int s21_decimal_is_negative(s21_decimal x) {
  return (s21_get_bit_32(x.bits[3], 31));
}

int s21_is_dec_even(s21_decimal dec) {
  int rem = 0;
  s21_div10_any_size(dec.bits, 3, dec.bits, 3, &rem);
  return ((rem + 1) % 2);
}

/**
 * @brief set POW of given decilal equal of given (short)pow
 */
int s21_set_pow(s21_decimal *x, unsigned short pow) {
  // int * dst_ptr = x->bits
  if (pow > 28) pow = 28;
  short int *dec_pow = (short int *)((x->bits) + 3);
  *(++dec_pow) = pow;

  return 0;
}
/**
 * @brief extract pow of given decimal
 */
unsigned short s21_decimal_get_pow(s21_decimal x) {
  return ((x.bits[3] & 0x001F0000) >> 16);
}

/**
 * @brief Count the number of bits set
 * @param *a pinter to uint32_t array
 * @param a_sz size of array
 * @return ammount of bits present in array
 * @returns int
 */
int s21_bit_count(uint32_t *a, size_t a_sz) {
  // s21_print_arrBits(a,a_sz);
  int counter = 0;
  for (int i = a_sz - 1; i >= 0; i--) {
    for (int j = 31; j >= 0; j--)
      if (s21_get_bit_32(a[i], (unsigned int)j)) counter++;
  }
  return counter;
}

int s21_highest_bit_present(uint32_t *a, size_t a_sz) {
  for (int i = a_sz - 1; i >= 0; i--) {
    for (int j = 31; j >= 0; j--)
      if (s21_get_bit_32(a[i], (uint32_t)j))
        return (i * 32 + j + 1);  //!! exit point
  }
  return 0;
}

void s21_arr_to_zero(uint32_t *arr, size_t sz) {
  for (int i = 0; i < (int)sz; i++) arr[i] = 0;
}

int s21_decimal_pow_equalizer(s21_decimal *a, s21_decimal *b) {
  short int pow_a = s21_decimal_get_pow(*a), pow_b = s21_decimal_get_pow(*b);
  if (pow_b > pow_a) return s21_decimal_pow_equalizer(b, a);
  int status = 0;
  uint32_t ten = 10;
  s21_decimal backup;
  while (pow_b < pow_a && !status) {
    backup = *b;
    status = s21_mul_any_size(b->bits, 3, &ten, 1, b->bits, 3);
    pow_b++;
  }

  if (status) {
    *b = backup;
    s21_set_pow(b, pow_b - 1);
  } else {
    s21_set_pow(b, pow_b);
  }
  return status;
}

/**
 * @return int
 * @retval 0 -> x != 0
 * @retval 1 -> x == 1
 * @note Zeroing pow and sign if x==0
 */
int s21_decimal_is_equal_to_zero(s21_decimal *x) {
  for (int i = 0; i < 3; i++)
    if (x->bits[i]) return 0;
  x->bits[3] =
      0;  //!! Zeroing pow and sign, but probably should not change anything
  return 1;
}

int s21_decimal_add_int(s21_decimal dec, int a, s21_decimal *res) {
  int err;
  s21_decimal plus_dec;
  int sign = (a < 0) ? 1 : 0;
  s21_decimal_set_to_zero(&plus_dec);
  if (sign) s21_negate(plus_dec, &plus_dec);
  plus_dec.bits[0] = abs(a);
  err = s21_add(dec, plus_dec, res);
  return err;
}

void s21_decimal_set_to_zero(s21_decimal *value) {
  value->bits[0] = value->bits[1] = value->bits[2] = value->bits[3] = 0;
}

int get_shift(s21_decimal val_1, s21_decimal val_2, int val_2_size) {
  int m = 0;
  int n = 0;
  bool break_val = false;
  for (int i = 2; i >= 0 && !break_val; i--) {
    for (int j = 31; j >= 0 && !break_val; j--) {
      if (val_1.bits[i] >> j & 1) {
        m = ((2 - i) * 32 + (32 - j));
        break_val = true;
      }
    }
  }
  break_val = false;
  if (val_2_size == 0) {
    for (int i = 2; i >= 0 && !break_val; i--) {
      for (int j = 31; j >= 0 && !break_val; j--) {
        if (val_2.bits[i] >> j & 1) {
          n = ((2 - i) * 32 + (32 - j));
          break_val = true;
        }
      }
    }
  } else {
    n = val_2_size;
  }
  return n - m;
}