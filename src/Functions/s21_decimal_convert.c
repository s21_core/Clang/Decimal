#include "../s21_decimal.h"

int s21_from_int_to_decimal(int src, s21_decimal *dst) {
  s21_decimal_set_to_zero(dst);
  if (src < 0) {
    src = s21_absi(src);
    dst->bits[3] |= (1 << 31);
  }
  memcpy((dst->bits), &src, sizeof(int));
  return 0;
}

int s21_from_float_to_decimal(float src, s21_decimal *dst) {
  int err = 0;
  s21_decimal_set_to_zero(dst);
  char num_str[100] = {0};
  sprintf(num_str, "%+.6e", src);
  int mantissa = 0;
  int i = 0;
  int is_exp = 0;
  int exp_sign = 1;
  int exp = 0;
  while (num_str[i]) {
    if (isdigit(num_str[i])) {
      if (!is_exp) {
        mantissa *= 10;
        mantissa += num_str[i] - '0';
      } else {
        exp *= 10;
        exp += num_str[i] - '0';
      }
    } else if (num_str[i] == 'e') {
      is_exp = 1;
      if (num_str[i + 1] == '-') exp_sign = -1;
    }
    i++;
  }
  exp *= exp_sign;
  exp -= 6;
  while (mantissa != 0 && exp < -28) {
    int remainder = mantissa % 10;
    mantissa /= 10;
    if (remainder >= 5) mantissa++;
    exp++;
  }
  if (exp < -28) {
    err = 1;
  }
  if (!(mantissa % 10)) {
    mantissa /= 10;
    exp++;
  }
  dst->bits[0] = mantissa;
  s21_decimal ten = {{10, 0, 0, 0}};
  while (exp > 0 && !err) {
    err = s21_mul(*dst, ten, dst);
    exp--;
  }
  if (err) {
    s21_decimal_set_to_zero(dst);
  } else {
    if (!s21_decimal_is_equal_to_zero(dst)) s21_set_pow(dst, abs(exp));
    if (num_str[0] == '-') s21_negate(*dst, dst);
  }
  return err;
}

int s21_from_decimal_to_int(s21_decimal src, int *dst) {
  int err = 0;
  int res = 0;
  int sign = (src.bits[3] >> 31 & 1) ? -1 : 1;
  s21_truncate(src, &src);
  if (src.bits[2] != 0 || src.bits[1] != 0) {
    err = 1;
  } else {
    res = src.bits[0];
    res *= sign;
  }
  *dst = res;
  return err;
}

int s21_from_decimal_to_float(s21_decimal src, float *dst) {
  int err = 0;
  double res = 0.0;
  int sign = (src.bits[3] >> 31 & 1) ? -1 : 1;
  for (int j = 0; j < 3; j++) {
    for (int i = 0; i < 32; i++) {
      if (j == 0 && i == 0)
        res += (double)(src.bits[j] & 1);
      else
        res += pow((src.bits[j] >> i & 1) * 2, i + j * 32);
    }
  }
  for (int i = s21_decimal_get_pow(src); i > 0; i--) res /= 10;
  res *= sign;
  *dst = (float)res;
  return err;
}
