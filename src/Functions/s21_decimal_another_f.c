#include "../s21_decimal.h"

int s21_floor(s21_decimal value, s21_decimal *result) {
  s21_decimal round_result;
  s21_decimal fractional_part;
  s21_decimal zero;
  s21_decimal_set_to_zero(&zero);
  s21_truncate(value, &round_result);
  s21_sub(value, round_result, &fractional_part);
  if (value.bits[3] >> 31) {
    s21_negate(fractional_part, &fractional_part);
    if (s21_is_greater(fractional_part, zero))
      s21_decimal_add_int(round_result, -1, &round_result);
  }
  *result = round_result;
  return 0;
}

int s21_round(s21_decimal value, s21_decimal *result) {
  s21_decimal round_result;
  s21_decimal fractional_part;
  s21_decimal half;
  s21_decimal_set_to_zero(&half);
  half.bits[0] = 5;
  s21_set_pow(&half, 1);
  s21_truncate(value, &round_result);
  s21_sub(value, round_result, &fractional_part);
  if (s21_decimal_is_negative(value)) {
    s21_negate(fractional_part, &fractional_part);
    if (s21_is_greater_or_equal(fractional_part, half))
      s21_decimal_add_int(round_result, -1, &round_result);
  } else {
    if (s21_is_greater_or_equal(fractional_part, half))
      s21_decimal_add_int(round_result, 1, &round_result);
  }
  *result = round_result;
  return 0;
}

int s21_truncate(s21_decimal value, s21_decimal *result) {
  s21_arr_to_zero(result->bits, 4);
  if (s21_decimal_is_negative(value)) s21_negate(*result, result);

  int val_pow = s21_decimal_get_pow(value);
  int rem = 0;
  for (int i = val_pow; i > 0; i--)
    s21_div10_any_size(value.bits, 3, value.bits, 3, &rem);
  memcpy(result->bits, value.bits, sizeof(uint32_t) * 3);

  return 0;
}

int s21_negate(s21_decimal value, s21_decimal *result) {
  value.bits[3] ^= 1 << 31;
  *result = value;
  return 0;
}