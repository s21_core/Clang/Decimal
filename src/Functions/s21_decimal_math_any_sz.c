#include "../s21_decimal.h"

/**
 * @brief add 2 multibyte arrays
 * @param res pointer to result array
 * @param res_sz size of result array
 * @param a pointer to first addendum array
 * @param a_sz size of first addendum array
 * @param b pointer to second addendum array
 * @param a_sz size of second addendum array
 *
 * @return (int) status of operation
 * @retval 0 - Success
 * @retval 1 - Overflow exceeded
 * @retval 2 - res have less bytes then a or b | size of 1 of variables is 0
 * @retval 3 - res|a|b has type s21_decimal but size of array > 3
 */
int s21_add_any_size(uint32_t *a, size_t a_sz, uint32_t *b, size_t b_sz,
                     uint32_t *res, size_t res_sz) {
  if (res_sz < b_sz || res_sz < a_sz || res_sz == 0 || a_sz == 0 || b_sz == 0)
    return 2;

  if ((is_decimal(a) && a_sz > 3) || (is_decimal(b) && b_sz > 3) ||
      (is_decimal(res) && res_sz > 3))
    return 3;

  uint32_t overflow = 0;
  uint32_t *a_cpy = calloc(a_sz, sizeof(uint32_t));
  uint32_t *b_cpy = calloc(b_sz, sizeof(uint32_t));
  if (a_cpy && b_cpy) {
    memcpy(a_cpy, a, a_sz * sizeof(uint32_t));
    memcpy(b_cpy, b, b_sz * sizeof(uint32_t));

    for (size_t bit_i = 0; bit_i < res_sz; bit_i++) {
      uint64_t intermediate_result =
          (uint64_t)overflow + (uint64_t)((a_sz > bit_i) ? (a_cpy[bit_i]) : 0) +
          (uint64_t)((b_sz > bit_i) ? (b_cpy[bit_i]) : 0);
      overflow = intermediate_result >> 32;
      res[bit_i] = (uint32_t)intermediate_result;
    }
    if (overflow) overflow = 1;
  } else
    overflow = 4;
  if (a_cpy) free(a_cpy);
  if (b_cpy) free(b_cpy);
  return overflow;
}
int s21_shift_bits_any_sz(uint32_t *a, size_t a_sz, int bit_shift) {
  if (bit_shift == 0) return 0;

  int overflow_err = 0;

  if (bit_shift > 0) {
    int bit_hop = bit_shift / 32;
    if (bit_hop > (int)a_sz - 1) return 1;

    bit_shift = bit_shift % 32;

    if (bit_hop) {
      for (int i = (a_sz - 1) - bit_hop; i > -1; i--) a[i + bit_hop] = a[i];
      for (int i = bit_hop - 1; i > -1; i--) a[i] = 0;
    }

    uint64_t tmp = 0;
    uint32_t owerflow = 0;

    for (int i = a_sz - 1; i >= 0 && !overflow_err; i--) {
      tmp = ((uint64_t)a[i] << bit_shift);
      owerflow = tmp >> 32;
      if (i == (int)a_sz - 1) {
        if (owerflow) overflow_err = 1;
      } else
        a[i + 1] |= owerflow;

      a[i] = (uint32_t)tmp;
    }
  } else {
    bit_shift = -bit_shift;

    int bit_hop = bit_shift / 32;
    if (bit_hop >= (int)a_sz)
      s21_arr_to_zero(a, a_sz);
    else {
      bit_shift = bit_shift % 32;

      if (bit_hop) {
        for (size_t i = bit_hop; i < a_sz; i++) a[i - bit_hop] = a[i];
        for (size_t i = a_sz - bit_hop; i < a_sz; i++) a[i] = 0;
      }

      uint64_t tmp = 0;
      uint32_t owerflow = 0;
      for (int i = 0; i < (int)a_sz; i++) {
        tmp = ((uint64_t)a[i] << 32);
        tmp = tmp >> bit_shift;
        owerflow = (uint32_t)tmp;
        a[i] = (uint32_t)(tmp >> 32);
        if (i != 0) a[i - 1] |= owerflow;
      }
    }
  }
  return overflow_err;
}

// probably fucked up when using same variable in res
int s21_mul_any_size(uint32_t *a, size_t a_sz, uint32_t *b, size_t b_sz,
                     uint32_t *res, size_t res_sz) {
  // decompose value2 (2^n + 2^n-1 ... 2^0)(already stored this way); sum
  // (shifted bits of value1 by n)

  if (res_sz < b_sz || res_sz < a_sz || res_sz == 0 || a_sz == 0 || b_sz == 0)
    return 2;

  if ((is_decimal(a) && a_sz > 3) || (is_decimal(b) && b_sz > 3) ||
      (is_decimal(res) && res_sz > 3))
    return 3;  // not working due cast on call

  if (s21_bit_count(a, a_sz) < s21_bit_count(b, b_sz))
    return s21_mul_any_size(b, b_sz, a, a_sz, res, res_sz);  //!!
  uint32_t *a_cpy = calloc(a_sz, sizeof(uint32_t));
  uint32_t *b_cpy = calloc(b_sz, sizeof(uint32_t));
  memcpy(a_cpy, a, a_sz * sizeof(uint32_t));
  memcpy(b_cpy, b, b_sz * sizeof(uint32_t));
  uint32_t *result = calloc(res_sz, sizeof(uint32_t));
  uint32_t *result_cpy = calloc(res_sz, sizeof(uint32_t));
  uint32_t *shifted_a = calloc(res_sz, sizeof(uint32_t));

  int shift_res = 0, add_res = 0;
  int definitely_not_a_break = 0;

  for (int i = b_sz - 1; i >= 0; i--) {
    for (int shift = 31; shift >= 0 && !definitely_not_a_break; shift--) {
      if ((b_cpy[i] >> shift) & 1) {
        int bit_shift = shift + 32 * i;
        s21_arr_to_zero(shifted_a, res_sz);
        memcpy(shifted_a, a_cpy, sizeof(uint32_t) * a_sz);
        memcpy(result_cpy, result, sizeof(uint32_t) * res_sz);
        if ((shift_res = s21_shift_bits_any_sz(shifted_a, res_sz, bit_shift)) ||
            (add_res = s21_add_any_size(result_cpy, res_sz, shifted_a, res_sz,
                                        result, res_sz))) {
          definitely_not_a_break = 1;
        }
      }
    }
  }

  if (!definitely_not_a_break) memcpy(res, result, res_sz * sizeof(uint32_t));
  free(result_cpy);
  free(a_cpy);
  free(b_cpy);
  free(shifted_a);
  free(result);

  return definitely_not_a_break;
}

int s21_div10_any_size(uint32_t *x, size_t x_size, uint32_t *result,
                       size_t r_size, int *remainder) {
  if (r_size < x_size) return 1;  //!=

  uint32_t *x_copy = calloc(x_size, sizeof(uint32_t));
  memcpy(x_copy, x, x_size * sizeof(uint32_t));

  s21_arr_to_zero(result, r_size);

  int x_h_bit = s21_highest_bit_present(x_copy, x_size);

  uint32_t *x_half = calloc(x_size, sizeof(uint32_t));
  uint32_t *x_quarter = calloc(x_size, sizeof(uint32_t));
  uint32_t *frac = calloc(r_size, sizeof(uint32_t));
  uint32_t *rem = calloc(r_size, sizeof(uint32_t));

  memcpy(x_half, x_copy, sizeof(uint32_t) * x_size);
  memcpy(x_quarter, x_copy, sizeof(uint32_t) * x_size);
  s21_shift_bits_any_sz(x_half, x_size, -1);
  s21_shift_bits_any_sz(x_quarter, x_size, -2);
  s21_add_any_size(x_half, x_size, x_quarter, x_size, result,
                   x_size);  // res = 0.75x

  for (int shift = 4; shift < x_h_bit; shift *= 2)  // || frac != 0
  {
    memcpy(frac, result, r_size * sizeof(uint32_t));
    s21_shift_bits_any_sz(frac, r_size, -shift);  // frac = res >> shift;
    s21_add_any_size(result, r_size, frac, r_size, result,
                     r_size);  // res+=frac;
  }

  s21_shift_bits_any_sz(result, r_size, -3);  // res = res >> 3;

  memcpy(rem, result,
         r_size * sizeof(uint32_t));      // (*4 + 1) * 2 == rem = resx10
  s21_shift_bits_any_sz(rem, r_size, 2);  // x4
  s21_add_any_size(rem, r_size, result, r_size, rem, r_size);  //+1
  s21_shift_bits_any_sz(rem, r_size, 1);                       // x2

  uint32_t one[1] = {1};
  for (int i = 0; i < (int)r_size; i++) rem[i] = ~rem[i];
  s21_add_any_size(rem, r_size, one, 1, rem, r_size);  // rem = -10 x res

  s21_add_any_size(rem, r_size, x_copy, x_size, rem, r_size);

  if (rem[0] > 9) {
    s21_add_any_size(result, r_size, one, 1, result, r_size);
    rem[0] -= 10;
  }
  *remainder = rem[0];
  free(rem);
  free(frac);
  free(x_half);
  free(x_quarter);
  free(x_copy);
  return 0;
}

int s21_pow_equalizer(uint32_t *a, int *pow_a, uint32_t *b, int *pow_b) {
  if (*pow_b > *pow_a) return s21_pow_equalizer(b, pow_b, a, pow_a);
  int status = 0;
  uint32_t ten = 10;
  while (*pow_b < *pow_a && !status) {
    status = s21_mul_any_size(b, 6, &ten, 1, b, 6);
    (*pow_b)++;
  }
  return status;
}
