#include "../s21_decimal.h"
#define OK 0
#define DIV_BY_ZERO 3

int s21_mul(s21_decimal value_1, s21_decimal value_2, s21_decimal *result) {
  // decompose value2 (2^n + 2^n-1 ... 2^0); sum (shifted bits of value1 by n)

  if (s21_bit_count(value_1.bits, 3) < s21_bit_count(value_2.bits, 3))
    return s21_mul(value_2, value_1, result);

  size_t big_res_sz = 2 * S21_DECIMAL_MANTISSA_SZ;
  uint32_t big_res[2 * S21_DECIMAL_MANTISSA_SZ] = {0};

  s21_arr_to_zero(result->bits, 4);
  int pow = s21_decimal_get_pow(value_1) + s21_decimal_get_pow(value_2);
  int sign =
      (s21_decimal_is_negative(value_1) != s21_decimal_is_negative(value_2));
  s21_mul_any_size(value_1.bits, S21_DECIMAL_MANTISSA_SZ, value_2.bits,
                   S21_DECIMAL_MANTISSA_SZ, big_res, big_res_sz);

  // pow and sign
  int status = s21_stick_into_decimal(big_res, big_res_sz, &pow, result);

  if (result->bits[0] == 0 && result->bits[1] == 0 && result->bits[2] == 0)
    result->bits[3] = 0;
  else if (pow < 0) {
    status = 1;
    if (sign) status++;
  } else {
    s21_set_pow(result, (short unsigned)pow);
    if (sign)
      s21_set_bit_32(&(result->bits[3]), 31);
    else
      s21_clear_bit_32(&(result->bits[3]), 31);
  }

  return status;
}

/**
 * @param a ponter to array of 32bit members
 * @param a_sz size of array
 * @param bit_shift gt 0 then a << bit_shift
 * @param bit_shift lt 0 then a >> bit_shift
 */

int s21_decimal_shift_bits(s21_decimal *decimal, int bit_shift) {
  return s21_shift_bits_any_sz(decimal->bits, 3, bit_shift);
}

int s21_div(s21_decimal value_1, s21_decimal value_2, s21_decimal *result) {
  int err = 0;
  s21_decimal_set_to_zero(result);
  if (s21_decimal_is_equal_to_zero(&value_2)) {
    err = DIV_BY_ZERO;
  } else {
    s21_decimal remainder;
    int sign_1 = value_1.bits[3] >> 31;
    int sign_2 = value_2.bits[3] >> 31;
    if (sign_1) s21_negate(value_1, &value_1);
    if (sign_2) s21_negate(value_2, &value_2);
    err = s21_decimal_pow_equalizer(&value_1, &value_2);
    s21_set_pow(&value_1, 0);
    s21_set_pow(&value_2, 0);
    bool val_1_is_less = true;
    int pow = 0;
    while (pow <= 29 && !err) {
      s21_decimal temp_value_2 = value_2;
      int k = get_shift(value_1, value_2, 0);
      s21_decimal intermediate_res;
      s21_decimal_set_to_zero(&intermediate_res);
      if (k >= 0) {
        int shift_res3;
        val_1_is_less = false;
        s21_decimal_set_to_zero(&remainder);
        s21_decimal_shift_bits(&value_2, k);
        s21_sub(value_1, value_2, &remainder);
        if (remainder.bits[3] >> 31)
          intermediate_res.bits[0] |= 0;
        else
          intermediate_res.bits[0] |= 1;
        for (int i = k; i > 0; i--) {
          s21_decimal remainder_backup = remainder;
          shift_res3 = s21_decimal_shift_bits(&remainder, 1);
          int lil_one = value_2.bits[0] & 1;
          s21_decimal value_2_backup = value_2;
          if (shift_res3) {
            remainder = remainder_backup;
            s21_decimal_shift_bits(&value_2, -1);
          }
          if (remainder.bits[3] >> 31) {
            s21_add(remainder, value_2, &remainder);
            if (shift_res3 && lil_one)
              s21_decimal_add_int(remainder, 1, &remainder);
          } else {
            s21_sub(remainder, value_2, &remainder);
            if (shift_res3 && lil_one)
              s21_decimal_add_int(remainder, -1, &remainder);
          }
          if (shift_res3) {
            s21_decimal two = {{2, 0, 0, 0}};
            s21_mul(remainder, two, &remainder);
            value_2 = value_2_backup;
          }
          if (remainder.bits[3] >> 31) {
            s21_decimal_shift_bits(&intermediate_res, 1);
            intermediate_res.bits[0] |= 0;
          } else {
            s21_decimal_shift_bits(&intermediate_res, 1);
            intermediate_res.bits[0] |= 1;
          }
        }
        if (remainder.bits[3] >> 31) s21_add(remainder, value_2, &remainder);
        s21_decimal_shift_bits(&remainder, -k);
      } else {
        if (val_1_is_less) remainder = value_1;
      }
      s21_decimal ten = {{10, 0, 0, 0}};
      err = s21_mul(remainder, ten, &remainder);
      if (!err) {
        s21_decimal backup_res = *result;
        if (pow < 29) {
          s21_set_pow(&intermediate_res, pow);
          err = s21_add(*result, intermediate_res, result);
        }
        if (err || pow == 29) {
          if (err) *result = backup_res;
          s21_set_pow(&intermediate_res, 0);
          s21_decimal five = {{5, 0, 0, 0}};
          s21_decimal one = {{1, 0, 0, 0}};
          s21_set_pow(&one, s21_decimal_get_pow(*result));
          if (s21_is_greater_or_equal(intermediate_res, five))
            s21_add(*result, one, result);
        }
        value_1 = remainder;
        value_2 = temp_value_2;
        if (s21_decimal_is_equal_to_zero(&remainder)) break;
      }
      pow++;
    }
    int rem = 0;
    s21_div10_any_size(result->bits, 3, value_1.bits, 3, &rem);
    while (rem == 0 && pow > 0) {
      int dev_null = 101010;
      s21_div10_any_size(result->bits, 3, result->bits, 3, &dev_null);
      pow = s21_decimal_get_pow(*result) - 1;
      s21_set_pow(result, pow);
      s21_div10_any_size(result->bits, 3, value_1.bits, 3, &rem);
    }
    if (sign_1 != sign_2) {
      s21_negate(*result, result);
      if (err) err = 2;
    }
    if (err && (pow > 0)) err = OK;
  }
  return err;
}

int s21_stick_into_decimal(uint32_t *arr, size_t arr_sz, int *pow,
                           s21_decimal *result) {
  int rem = 0;
  int rem_plus_one = 0;
  while (arr[3] || arr[4] || arr[5] ||
         ((*pow > 28) && (arr[0] || arr[1] || arr[2]))) {  // truncate
    rem_plus_one = rem;
    s21_div10_any_size(arr, arr_sz, arr, arr_sz, &rem);
    if (rem == 0) rem = (rem_plus_one) ? 1 : 0;
    (*pow)--;
  }
  memcpy(result->bits, arr, S21_DECIMAL_MANTISSA_SZ * sizeof(uint32_t));

  if (rem == 5) rem += rem_plus_one;
  int status = s21_decimal_banking_round_to_fit(result, rem);
  s21_set_pow(result, *pow);
  return status;
}

int s21_decimal_banking_round_to_fit(s21_decimal *dec, int rem) {
  int status = 0;
  if ((rem > 5) || ((rem == 5) && !s21_is_dec_even(*dec))) {
    uint32_t one = 1;
    status = s21_add_any_size(dec->bits, 3, &one, 1, dec->bits, 3);  // add one
  }
  return status;
}

void s21_true_negate_big(uint32_t *arr) {
  uint32_t one = 1;
  for (int i = 0; i < 6; i++) arr[i] = ~arr[i];
  s21_toggle(&arr[6], 0);

  s21_add_any_size(arr, 7, &one, 1, arr, 7);
}

int s21_add(s21_decimal value_1, s21_decimal value_2,
            s21_decimal *result) {  // works but round test failed
  s21_arr_to_zero(result->bits, 4);

  int pow_1 = s21_decimal_get_pow(value_1);
  int pow_2 = s21_decimal_get_pow(value_2);

  uint32_t a[7] = {0};  // 6 bytes for digits 7B0b for sign
  uint32_t b[7] = {0};
  uint32_t res[7] = {0};

  memcpy(a, value_1.bits, sizeof(uint32_t) * 3);
  memcpy(b, value_2.bits, sizeof(uint32_t) * 3);
  int sign = 0;
  int status = 0;
  if (!(status = s21_pow_equalizer(a, &pow_1, b, &pow_2))) {
    if (!status) {
      if (s21_decimal_is_negative(value_1)) s21_true_negate_big(a);
      if (s21_decimal_is_negative(value_2)) s21_true_negate_big(b);
      status = s21_add_any_size(a, 7, b, 7, res, 7);
      if (s21_get_bit_32(res[6], 0)) {
        s21_true_negate_big(res);
        sign = -1;
      }
      if (!status) status = s21_stick_into_decimal(res, 6, &pow_1, result);
      if (sign) s21_negate(*result, result);
    }
  }
  return status;
}

int s21_sub(s21_decimal value_1, s21_decimal value_2, s21_decimal *result) {
  s21_negate(value_2, &value_2);
  return s21_add(value_1, value_2, result);
}
